# Dark DB-Infoscreen

Based on [dbf](https://dbf.finalrewind.org/).

---

### Local Set-up
1. Install dependencies:
    1. Verify that Python >= 3.5 is available
    2. `pip install -r requirements.txt`
2. Start via Flask command
    - `FLASK_APP=dark_dbf`
    - `flask run`

### Additional Notes for Development
##### Regarding Style-Changes
1. [Install Sass](http://sass-lang.com/install)
2. Get Bulma-Sources:
    1. `git submodule init`
    2. `git submodule update`

- Use `static\style\build.sh` to update the served stylesheet.

### Deployment Set-Up
1. login to the cluster on your shell, than run `oc process -f deployment-template.yaml | oc apply -f -`
1. configure a web hook in the GitLab repo, copying the target URL from the settings of the `dbf` BuildConfig in OpenShift


---


### Contributing
I'm open for all Forks, Feedback and Pull Requests.

### License
This project is licensed under the terms of the *GNU General Public License v3.0*. For further information, please look [here](http://choosealicense.com/licenses/gpl-3.0/) or [here<sup>(DE)</sup>](http://www.gnu.org/licenses/gpl-3.0.de.html).
