from typing import Union


class ShortEntry:
    def __init__(self,
        number: str, name: str, class_: str, destination: str, via: str,
        scheduled_arrival: Union[str, None], scheduled_departure: Union[str, None], scheduled_platform: str,
        delay_arrival: Union[int, None], delay_departure: Union[str, None], actual_platform: str,
        is_cancelled: bool, has_messages: bool
    ):
        self.number = number
        self.name = name
        self.class_ = class_
        self.destination = destination
        self.via = via
        self.scheduled_arrival = scheduled_arrival
        self.scheduled_departure = scheduled_departure
        self.scheduled_platform = scheduled_platform
        self.delay_arrival = delay_arrival
        self.delay_departure = delay_departure
        self.actual_platform = actual_platform
        self.is_cancelled = is_cancelled
        self.has_messages = has_messages

    actual_arrival = property(fget=lambda self: _calc_time(self.scheduled_arrival, self.delay_arrival))
    actual_departure = property(fget=lambda self: _calc_time(self.scheduled_departure, self.delay_departure))
    is_changed_arrival = property(fget=lambda self: self.delay_arrival is not None and self.delay_arrival != 0)
    is_changed_departure = property(fget=lambda self: self.delay_departure is not None and self.delay_departure != 0)


def _calc_time(scheduled: str, delay: str) -> Union[str, None]:
    if scheduled is None:
        return None

    if delay is None:
        delay = 0

    delay_i = int(delay) if delay is not None else 0
    delay_i_hours = delay_i // 60
    delay_i_minutes = delay_i % 60

    (scheduled_i_hours, scheduled_i_minutes) = [int(part) for part in scheduled.split(':')]

    actual_i_hours = scheduled_i_hours + delay_i_hours
    actual_i_minutes = scheduled_i_minutes + delay_i_minutes
    if actual_i_minutes >= 60:
        actual_i_minutes -= 60
        actual_i_hours += 1
    actual_i_hours %= 24

    return f'{actual_i_hours:02d}:{actual_i_minutes:02d}'
