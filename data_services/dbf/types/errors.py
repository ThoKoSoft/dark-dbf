class UnknownStationError(ValueError):
    pass


class UnknownEntryError(ValueError):
    pass