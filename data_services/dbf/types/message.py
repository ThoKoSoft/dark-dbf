from .message_type import MessageType


class Message:
    def __init__(self, text: str, time: str, type_: MessageType):
        self.text = text
        self.time = time
        self.type_ = type_
