import requests

from .types.errors import UnknownStationError, UnknownEntryError
from .types.full_entry import FullEntry
from .types.message import Message
from .types.message_type import MessageType
from .types.short_entry import ShortEntry
from .types.stop import Stop


def get_data(bs_abbrev: str) -> [dict]:
    payload = {'mode': 'json', 'version': '3'}
    r = requests.get('https://dbf.finalrewind.org/' + bs_abbrev, params=payload)

    r.raise_for_status()
    data = r.json()

    if data.get('error', '') == 'Ambiguous station name':
        raise UnknownStationError

    return data.get('departures', [])

def parse_to_entry_list(data_list: [dict]) -> [ShortEntry]:
    departures_entries = []

    for data in data_list:
        departures_entries.append(_parse_to_short_entry(data))

    return departures_entries

def parse_to_one_entry(data_list: [dict], desired_number: str) -> FullEntry:
    for data in data_list:
        if data.get('trainNumber') != desired_number:
            continue

        short = _parse_to_short_entry(data)

        stops_data = data.get('route', [])
        stops = [
            Stop(
                stop_entry.get('name', 'n/a'),
                _is(stop_entry, 'isAdditional'),
                _is(stop_entry, 'isCancelled')
            ) for stop_entry in stops_data
        ]

        messages_data = data.get('messages', {'delay': [], 'qos': []})
        messages = [
            Message(
                m.get('text'),
                m.get('timestamp'),
                MessageType.delay
            ) for m in messages_data.get('delay')
        ]
        messages += [
            Message(
                m.get('text'),
                m.get('timestamp'),
                MessageType.quality_of_service
            ) for m in messages_data.get('qos')
        ]
        for message in messages:
            message.time = ':'.join(message.time.split('T')[-1].split(':')[0:2])
        messages = sorted(messages, key=lambda m: m.time)

        return FullEntry(
            short, stops, messages
        )

    raise UnknownEntryError()

def parse_to_message_list(data_list: [dict]) -> [str]:
    messages = []

    for data in data_list:
        messages_data = data.get('messages', {'delay': []})
        messages += [
            m.get('text')
            for m in messages_data.get('delay')
        ]

    return messages


def _parse_to_short_entry(data: dict) -> ShortEntry:
    number = data.get('trainNumber', 'n/a')

    name = data.get('train', 'n/a')

    train_classes = data.get('trainClasses', [''])
    if len(train_classes) < 1:  # as it can be an empty array from the side of the API
        train_class = ''
    else:
        train_class = train_classes[0]

    destination = data.get('destination', 'n/a')

    via_str = ', '.join(data.get('via', []))

    scheduled_arrival = data.get('scheduledArrival')
    scheduled_departure = data.get('scheduledDeparture')
    scheduled_platform = data.get('scheduledPlatform')
    delay_arrival = data.get('delayArrival')
    delay_departure = data.get('delayDeparture')
    actual_platform = data.get('platform')

    is_cancelled = _is(data, 'isCancelled')

    messages_data = data.get('messages', {'delay': [], 'qos': []})
    has_messages = len(messages_data.get('delay')) > 0 or len(messages_data.get('qos')) > 0

    return ShortEntry(
        number, name, train_class, destination, via_str,
        scheduled_arrival, scheduled_departure, scheduled_platform,
        delay_arrival, delay_departure, actual_platform,
        is_cancelled, has_messages
    )

def _is(obj: dict, key: str) -> bool:
    value = obj.get(key)
    try:
        return int(value) == 1
    except TypeError:
        return False
