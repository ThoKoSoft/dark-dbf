import os
import csv
import re

from data_services.abbrev.types.abbrev_entry import AbbrevEntry

# region init
FILE_NAME = 'DBNetz-Betriebsstellenverzeichnis-Stand2018-04.csv'

with open(os.path.join(os.path.dirname(__file__), FILE_NAME), 'r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    data = [AbbrevEntry(row['Abk'], row['Name'], row['Typ']) for row in reader]
# endregion


def get_name(abbrev: str) -> str:
    for row in data:
        if row.abbrev == abbrev:
            return row.name

    return 'n/a'

def list_matching_entries(query: str) -> [AbbrevEntry]:
    query_re = '.*' + re.sub('[ \-*]', '.*', query.lower()) + '.*'
    matches = []

    for row in data:
        row_name = row.name.lower()
        if re.search(query_re, row_name):
            if row.typ in ['Bf', 'Bft', 'Hp', 'NE-Bf', 'NE-Bft', 'NE-Hp']:
                # Bf = Bahnhof, Bft = Bahnhofsteil, Hp = Haltepunkt, NE-* = Nicht eigene - *
                matches.append(
                    AbbrevEntry(row.abbrev, row.name, row.typ)
                )

    return matches
