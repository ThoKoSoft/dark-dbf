class Statistic:
    def __init__(self,
        delay_avg: int, delay_top: int,
        delay_count_good: int, delay_count_slightly: int, delay_count_much: int, delay_count_extreme: int,
        delay_count_canceled: int,
        top_delay_messages: [tuple]
    ):
        self.delay_avg = delay_avg
        self.delay_top = delay_top
        self.delay_count_good = delay_count_good
        """ 0min <= delay < 1nin """
        self.delay_count_slightly = delay_count_slightly
        """ 1min <= delay < 6min """
        self.delay_count_much = delay_count_much
        """ 6min <= delay < 60min """
        self.delay_count_extreme = delay_count_extreme
        """ 60min <= delay """
        self.delay_count_canceled = delay_count_canceled
        self.top_delay_messages = top_delay_messages
