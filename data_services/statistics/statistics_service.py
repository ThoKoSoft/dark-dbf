from collections import Counter

from data_services.dbf import dbf_service
from .types.statistic import Statistic


def create_statistics(data_list: [dict]) -> Statistic:
    entries = dbf_service.parse_to_entry_list(data_list)

    delay_count_canceled = 0
    delays = []
    for entry in entries:
        if entry.is_cancelled:
            delay_count_canceled += 1
            continue
        delay = entry.delay_departure
        if delay is None:
            delay = entry.delay_arrival
        delays.append(delay)

    delays = sorted(delays, reverse=True)

    delay_avg = sum(delays) / float(len(delays))
    delay_top = delays[0]

    delay_count_good = 0
    delay_count_slightly = 0
    delay_count_much = 0
    delay_count_extreme = 0
    try:
        next_delay = delays.pop()
        while next_delay < 1:
            delay_count_good += 1
            next_delay = delays.pop()
        while next_delay < 6:
            delay_count_slightly += 1
            next_delay = delays.pop()
        while next_delay < 60:
            delay_count_much += 1
            next_delay = delays.pop()
        delay_count_extreme = len(delays) + 1  # as we popped the last
    except IndexError:  # = exhausted list before reaching end
        pass

    delay_messages = dbf_service.parse_to_message_list(data_list)
    sorted_delay_messages = sorted(
        Counter(delay_messages).items(), key=lambda kv: kv[1], reverse=True
    )  # we need to set reverse as per default the sort direction is ascending
    top_delay_messages = sorted_delay_messages[:5]

    return Statistic(
        delay_avg,
        delay_top,
        delay_count_good,
        delay_count_slightly,
        delay_count_much,
        delay_count_extreme,
        delay_count_canceled,
        top_delay_messages
    )
