from data_services.wagon.types.wagon_speciality import WagonSpeciality


class Wagon:
    def __init__(self,
                 number: str, sector: str,
                 seat_class: str, specialities: [WagonSpeciality],
                 position: int, is_control: bool
    ):
        self.number = number
        self.sector = sector
        self.seat_class = seat_class
        self.specialities = specialities
        self.position = position
        """1: first, -1: last, 0: between"""
        self.is_control = is_control
