import requests


# region init
r = requests.get('https://api.railway-stations.org/de/stations')

r.raise_for_status()
raw_data = r.json()
data = dict([(entry.get('DS100'), entry.get('photoUrl', None)) for entry in raw_data])
# endregion

def get_photo_url(abbrev: str) -> str:
    return data.get(abbrev, '')
