from flask import Flask, render_template, send_from_directory, request, make_response, redirect, url_for

import controllers


def create_app():
    app = Flask(__name__)

    # region static
    @app.route('/favicon.ico')
    def route_favicon():
        return send_from_directory('static', filename='media/favicon.ico')

    @app.route('/dark-dbf.webmanifest')
    def route_manifest():
        return send_from_directory('static', filename='dark-dbf.webmanifest')

    @app.route('/appicon.png')
    def route_appicon():
        return send_from_directory('static', filename='media/appicon.png')

    @app.route('/app.js')
    def route_app_js():
        return send_from_directory('static', filename='app.js')

    @app.route('/worker.js')
    def route_worker_js():
        return send_from_directory('static', filename='worker.js')
    # endregion

    # region error pages
    @app.errorhandler(500)
    def route_error_500(e):
        return (
            render_template(
                'error.jinja2',
                title='Error 500',
                msg='interner Fehler',
                msg_internal=e.__class__.__name__ + ' - ' + str(e)
            ),
            500
        )
    # endregion

    # region main routes
    @app.route('/')
    def route_home():
        mode = request.args.get('mode', '')
        if mode == 'search':
            return controllers.home_search()
        if mode == 'abbrev':
            return make_response(redirect(url_for(
                'route_station', bs_abbrev=request.args.get('abbrev', '')
            )))
        else:
            return controllers.home()

    @app.route('/<bs_abbrev>')
    def route_station(bs_abbrev: str):
        bs_abbrev = bs_abbrev.upper()
        mode = request.args.get('mode', '')
        if mode == 'addFav':
            return controllers.station_fav_add(bs_abbrev)
        elif mode == 'removeFav':
            return controllers.station_fav_remove(bs_abbrev)
        elif mode == 'details':
            return controllers.station_details(bs_abbrev)
        elif mode == 'statistics':
            return controllers.station_statistics(bs_abbrev)
        elif mode == 'dashboard':
            return controllers.station_dashboard(bs_abbrev)
        else:
            return controllers.station_list(bs_abbrev)

    @app.route('/<bs_abbrev>/<entry_number>')
    def route_details(bs_abbrev: str, entry_number: str):
        bs_abbrev = bs_abbrev.upper()
        return controllers.entry_details(bs_abbrev, entry_number)
    # endregion

    return app
